var user = "";

function Loadchannels() {
    var updatePage = function(resp) {
        for (var i = 0; i < resp.resources.length; i++) {
            console.log(resp.resources[0].name);
            $('#channel_list').append($('<a>')
                .attr("href", "file:///home/alfa/chatapp/index.html#" + resp.resources[i].id)
                .attr('onclick', "javascript:Load_msg(" + resp.resources[i].id + ")")
                .append($('<div>').attr('id', resp.resources[i].id)
                    .addClass("channelblocks").text(resp.resources[i].id + "          " + resp.resources[i].name)));
        };
    }
    var printError = function(req, status, err) {
        console.log('something went wrong', status, err);
    };
    var ajaxOptions = {
        url: 'http://0.0.0.0:5000/channel/',
        dataType: 'json',
        success: updatePage,
        error: printError
    };
    $.ajax(ajaxOptions);
}

function Get_msgs(n) {
    var updatePage = function(msgs) {
        for (var i = 0; i < msgs.resources.length; i++) {
            if (msgs.resources[i].fid == n) {
                $('#msg_block').append("<div  id= user >" + msgs.resources[i].user + "</div");
                $('#msg_block').append("<div  id= message_style >" + msgs.resources[i].msg + "</div");

            }
        }
    }
    var printError = function(req, status, err) {
        console.log('something went wrong', status, err);
    };
    var ajaxOptions = {
        url: 'http://0.0.0.0:5000/messages/',
        dataType: 'json',
        success: updatePage,
        error: printError
    };
    $.ajax(ajaxOptions);

}

function Load_msg(n) {
    $('#msg_block').empty();
    $('.heighlight').removeClass('heighlight');
    $('#' + n).addClass('heighlight');
    Get_msgs(n);
}


function Loadlastchannel() {
    var updatePage = function(resp) {
        console.log(resp.resources[resp.resources.length - 1].name);
        console.log("mdkfslnj");
        $('#channel_list').append($('<a>')
            .attr("href", "file:///home/alfa/chatapp/index.html#" + resp.resources[resp.resources.length - 1].id)
            .attr('onclick', "javascript:Load_msg(" + resp.resources[resp.resources.length - 1].id + ")")
            .append($('<div>').attr('id', resp.resources[resp.resources.length - 1].id)
                .addClass("channelblocks").text(resp.resources[resp.resources.length - 1].id + "          " + resp.resources[resp.resources.length - 1].name)));
    }
    var printError = function(req, status, err) {
        console.log('something went wrong', status, err);
    };
    var ajaxOptions = {
        url: 'http://0.0.0.0:5000/channel/',
        dataType: 'json',
        success: updatePage,
        error: printError
    };
    $.ajax(ajaxOptions);
}

function Login(showhide) {
    if (showhide == "show") {
        $("#popupbox").removeClass('hidden');
        $('#msg_block').empty();

    } else if (showhide == "hide") {
        $("#popupbox").addClass('hidden');
        $("#login").addClass('hidden');
        $("#logout").removeClass('hidden');
        Loadchannels();
        user = $("input")[0].value;
    }
}

function Create_channel(showhide) {
    if (showhide == "show") {
        $("#channelbox").removeClass('hidden');

    } else if (showhide == "hide") {
        $("#channelbox").addClass('hidden');
        Add_channel();

    }
}

function Send_message() {
    var msg = $('.message_input')[0].value;
    console.log(msg);


    var query = window.location.href;
    var vars = query.split("#");
    var id = vars[1];
    console.log(id);
    var updatePage = function(resp) {
        console.log(resp);

        var ajaxOptions = {
            type: 'POST',
            url: 'http://0.0.0.0:5000/broadcast',
            data: JSON.stringify({ "fid": id, "msg": msg, "user": user }),
            dataType: 'json',
            contentType: 'application/json',
        };
        $.ajax(ajaxOptions);
    }
    var printError = function(req, status, err) {
        console.log('something went wrong', status, err);
    };
    var ajaxOptions = {
        type: 'POST',
        url: 'http://0.0.0.0:5000/messages/',
        data: JSON.stringify({ "fid": id, "msg": msg, "user": user }),
        dataType: 'json',
        success: updatePage,
        error: printError
    };
    $.ajax(ajaxOptions);

}

function Add_channel() {
    var name = $('input')[2].value;
    var updatePage = function(resp) {
        console.log(resp);
        Loadlastchannel();
    }
    var printError = function(req, status, err) {
        console.log('something went wrong', status, err);
    };
    var ajaxOptions = {
        type: 'POST',
        url: 'http://0.0.0.0:5000/channel/',
        data: JSON.stringify({ "name": name }),
        dataType: 'json',
        success: updatePage,
        error: printError
    };
    $.ajax(ajaxOptions);
}

function Logout() {
    $("#logout").addClass('hidden');
    $("#login").removeClass('hidden');
    $(".channelblocks").remove();
    $('#msg_block').empty();
    user = "";
}